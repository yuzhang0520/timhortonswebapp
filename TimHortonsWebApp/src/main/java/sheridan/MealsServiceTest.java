package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDrinksRegular() {
		assertTrue("Types wrong", MealsService.getAvailableMealTypes(MealType.DRINKS).size() > 0);
	}
	
	@Test
	public void testDrinksException() {
		assertTrue("Types wrong", MealsService.getAvailableMealTypes(null).size() > 0);
	}
	
	@Test
	public void testDrinksBoundaryIn() {
		assertTrue("Types wrong", MealsService.getAvailableMealTypes(MealType.DRINKS).size() > 3);
	}
	
	@Test
	public void testDrinksBoundaryOut() {
		assertTrue("Types wrong", MealsService.getAvailableMealTypes(null).size() <= 1);
	}

}
